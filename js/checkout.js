(function ($, Drupal, once) {
  Drupal.behaviors.mercadopago_checkout = {
    attach: function (context, settings) {
      $('.commerce-checkout-flow-multistep-default', context).submit(function(e) {
        e.preventDefault();
      });

      once('mercadopago_checkout', '.mercadopago-buttons-container', context).forEach(function(el) {
        var preference_id = $(el).data('preference-id');
        const mp = new MercadoPago(drupalSettings.mercadopago_checkout.public_key);
        const checkeout = mp.checkout({
          preference: {
            id: preference_id
          },
          render: {
            container: '.mercadopago-buttons-container',
            label: Drupal.t('Pay')
          }
        })
      });


    }
  };
})(jQuery, Drupal, once);
