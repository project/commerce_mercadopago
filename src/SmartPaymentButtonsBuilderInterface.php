<?php

namespace Drupal\commerce_mercadopago;

use Drupal\commerce_payment\Entity\PaymentInterface;

/**
 * Provides an interface for the Smart payment buttons builder.
 */
interface SmartPaymentButtonsBuilderInterface {

  /**
   * Builds the Smart payment buttons.
   *
   * @param \Drupal\commerce_payment\Entity\PaymentInterface $payment
   *   The payment.
   * @param string $return_url
   *   The URL to redirect after completing the payment.
   * @param bool $commit
   *   Set to TRUE if the transaction is Pay Now, or FALSE if the amount
   *   captured changes after the buyer returns to your site.
   *   (To be implemented).
   *
   * @return array
   *   A renderable array representing the Smart payment buttons.
   */
  public function build(PaymentInterface $payment, string $return_url, bool $commit);

}
