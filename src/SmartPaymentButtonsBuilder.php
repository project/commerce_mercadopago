<?php

namespace Drupal\commerce_mercadopago;

use Drupal\commerce_payment\Entity\PaymentInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Url;
use MercadoPago\Item;
use MercadoPago\Preference;
use MercadoPago\SDK;

/**
 * Provides a helper for building the Smart payment buttons.
 */
class SmartPaymentButtonsBuilder implements SmartPaymentButtonsBuilderInterface {

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Constructs a new SmartPaymentButtonsBuilder object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The configuration factory.
   */
  public function __construct(ConfigFactoryInterface $config_factory) {
    $this->configFactory = $config_factory;
  }

  /**
   * {@inheritdoc}
   */
  public function build(PaymentInterface $payment, string $return_url, bool $commit) {
    $order = $payment->getOrder();

    $payment_gateway = $payment->getPaymentGateway();

    $config = $payment_gateway->getPluginConfiguration();

    SDK::setAccessToken($config['private_key']);

    $preference = new Preference();

    $notification_url = Url::fromRoute('commerce_payment.notify', [
      'commerce_payment_gateway' => $payment_gateway->id(),
    ], ['absolute' => TRUE]);

    $preference->back_urls = [
      'success' => $return_url,
      // 'failure' => '',
      'pending' => $return_url,
    ];

    $preference->external_reference = $order->id();
    $preference->notification_url = $notification_url->toString();

    $items = [];
    foreach ($order->getItems() as $item) {
      $remote_item = new Item();
      $remote_item->title = $item->getTitle();
      $remote_item->quantity = (int) $item->getQuantity();
      $remote_item->unit_price = $item->getUnitPrice()->getNumber();
      $remote_item->currency_id = $item->getUnitPrice()->getCurrencyCode();
      $items[] = $remote_item;
    }
    $preference->items = $items;
    $preference->save();
    $preference_id = $preference->id;

    $order->setData('commerce_mercadopago_checkout', [
      'preference_id' => $preference_id,
    ]);
    $order->save();

    $element = [
      '#type' => 'html_tag',
      '#tag' => 'div',
      '#weight' => 100,
      '#value' => "",
      '#attached' => [
        'library' => [
          'commerce_mercadopago/checkout',
        ],
        'drupalSettings' => [
          'mercadopago_checkout' => [
            'public_key' => $config['api_key'],
          ],
        ],
      ],
      '#attributes' => [
        'class' => ['mercadopago-buttons-container'],
        'data-preference-id' => $preference_id ?? 'test',
      ],
    ];
    return $element;
  }

}
