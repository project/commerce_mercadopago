<?php

namespace Drupal\commerce_mercadopago\Plugin\Commerce\PaymentGateway;

use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_payment\Entity\PaymentInterface;
use Drupal\commerce_payment\Exception\PaymentGatewayException;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\OffsitePaymentGatewayBase;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\SupportsRefundsInterface;
use Drupal\commerce_price\Price;
use Drupal\Core\Form\FormStateInterface;
use MercadoPago\MerchantOrder;
use MercadoPago\Payment;
use MercadoPago\Preference;
use MercadoPago\Refund;
use MercadoPago\SDK;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Provides the QuickPay offsite Checkout payment gateway.
 *
 * @CommercePaymentGateway(
 *   id = "mercadopago_redirect_checkout",
 *   label = @Translation("MercadoPago (Redirect to mercadopago)"),
 *   display_label = @Translation("Mercado Pago"),
 *    forms = {
 *     "offsite-payment" = "Drupal\commerce_mercadopago\PluginForm\Checkout\PaymentOffsiteForm",
 *   },
 *   payment_method_types = {"credit_card"},
 *   credit_card_types = {
 *     "mastercard", "visa",
 *   },
 * )
 */
class MercadoPago extends OffsitePaymentGatewayBase implements SupportsRefundsInterface {

  /**
   * The logger factory.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  protected $loggerFactory;

  /**
   * The request stack.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    $instance->loggerFactory = $container->get('logger.factory');
    $instance->requestStack = $container->get('request_stack');

    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'private_key' => '',
      'api_key' => '',
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    $form['private_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Private key'),
      '#description' => $this->t('This is the private key from the MercadoPago manager.'),
      '#default_value' => $this->configuration['private_key'],
      '#required' => TRUE,
    ];

    $form['api_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('API key'),
      '#description' => $this->t('The API key for the same user as used in Agreement ID.'),
      '#default_value' => $this->configuration['api_key'],
      '#required' => TRUE,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);
    $values = $form_state->getValue($form['#parents']);
    $this->configuration['private_key'] = $values['private_key'];
    $this->configuration['api_key'] = $values['api_key'];
  }

  /**
   * {@inheritdoc}
   */
  public function createPayment(PaymentInterface $payment, $capture = TRUE) {
    SDK::setAccessToken($this->configuration['private_key']);
  }

  /**
   * {@inheritdoc}
   */
  public function capturePayment(PaymentInterface $payment, Price $amount = NULL) {
    exit;
  }

  /**
   * {@inheritdoc}
   */
  public function voidPayment(PaymentInterface $payment) {
    exit;
  }

  /**
   * {@inheritdoc}
   */
  public function refundPayment(PaymentInterface $payment, Price $amount = NULL) {
    SDK::setAccessToken($this->configuration['private_key']);

    $amount = $amount ?: $payment->getAmount();

    $refund = new Refund([
      "payment_id" => $payment->getRemoteId(),
    ]);

    $refund->amount = floatval($amount->getNumber());

    $result = $refund->save();

    if (!$result) {
      throw new PaymentGatewayException($refund->error);
    }

    $payment->setRefundedAmount($payment->getAmount());
    $payment->setState('refunded');
    $payment->save();
  }

  /**
   * {@inheritdoc}
   */
  public function onNotify(Request $request) {
    SDK::setAccessToken($this->configuration['private_key']);
    $currentRequest = $this->requestStack->getCurrentRequest();
    $queryParameters = $currentRequest->query->all();

    $this->loggerFactory->get('mercadopago_checkout')->info(print_r($queryParameters, 1));

    if ($queryParameters['type'] == 'payment') {
      $mp_payment = Payment::find_by_id($request->query->get('data_id'));

      if ($mp_payment->status != 'approved') {
        return;
      }

      $order_id = $mp_payment->external_reference;

      $payment_storage = $this->entityTypeManager->getStorage('commerce_payment');

      $payment_ids = $payment_storage->getQuery()
        ->condition('order_id', $order_id)
        ->accessCheck(FALSE)
        ->sort('payment_id', 'DESC')
        ->range(0, 1)
        ->execute();

      if (!$payment_ids) {
        $order = $this->entityTypeManager->getStorage('commerce_order')->load($order_id);

        $payment_method = NULL;

        if (!$order->get('payment_method')->isEmpty()) {
          /** @var \Drupal\commerce_payment\Entity\PaymentMethodInterface $payment_method */
          $payment_method = $order->get('payment_method')->entity;
        }
        // If the order doesn't reference a payment method yet, or if
        // the payment method doesn't reference the right gateway,
        // create a new one.
        if (!$payment_method || $payment_method->getPaymentGatewayId() !== $this->parentEntity->id()) {
          // Create a payment method.
          $payment_method_storage = $this->entityTypeManager->getStorage('commerce_payment_method');
          $payment_method = $payment_method_storage->createForCustomer(
            'mercadopago_checkout',
            $this->parentEntity->id(),
            $order->getCustomerId(),
            $order->getBillingProfile()
          );
        }
        $payment_storage = $this->entityTypeManager->getStorage('commerce_payment');

        $payment = $payment_storage->create([
          'payment_gateway' => $this->parentEntity->id(),
          'payment_method' => $payment_method->id(),
          'order_id' => $order_id,
        ]);
      }
      else {
        $payment = $payment_storage->load(reset($payment_ids));
      }

      $amount = Price::fromArray([
        'number' => $mp_payment->transaction_amount,
        'currency_code' => $mp_payment->currency_id,
      ]);

      $payment->setAmount($amount);
      $payment->setRemoteId($request->query->get('data_id'));
      $payment->setState('completed');
      $payment->setRemoteState($mp_payment->status);
      $payment->save();
    }

    if ($queryParameters['topic'] == 'merchant_order') {
      $merchant_order = MerchantOrder::find_by_id($request->query->get('id'));
      $preference = Preference::find_by_id($merchant_order->preferenceId);
      $this->loggerFactory->get('mercadopago_checkout')->info('External reference: ' . $preference->external_reference);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function onReturn(OrderInterface $order, Request $request) {
    if ($request->query->get('status') == 'rejected') {
      return;
    }

    SDK::setAccessToken($this->configuration['private_key']);
    $payment_storage = $this->entityTypeManager->getStorage('commerce_payment');
    $payment_ids = $payment_storage->getQuery()
      ->condition('order_id', $order->id())
      ->condition('state', 'completed')
      ->accessCheck(FALSE)
      ->sort('payment_id', 'DESC')
      ->range(0, 1)
      ->execute();

    if (count($payment_ids) > 0) {
      return;
    }

    $payment_method = NULL;
    // If a payment method is already referenced by the order, no need to create
    // a new one.
    if (!$order->get('payment_method')->isEmpty()) {
      /** @var \Drupal\commerce_payment\Entity\PaymentMethodInterface $payment_method */
      $payment_method = $order->get('payment_method')->entity;
    }
    // If the order doesn't reference a payment method yet, or if the payment
    // method doesn't reference the right gateway, create a new one.
    if (!$payment_method || $payment_method->getPaymentGatewayId() !== $this->parentEntity->id()) {
      // Create a payment method.
      $payment_method_storage = $this->entityTypeManager->getStorage('commerce_payment_method');
      $payment_method = $payment_method_storage->createForCustomer(
        'mercadopago_checkout',
        $this->parentEntity->id(),
        $order->getCustomerId(),
        $order->getBillingProfile()
      );
    }
    $payment_method->setRemoteId($request->query->get('merchant_order_id'));
    $payment_method->setReusable(FALSE);
    $payment_method->save();

    $order->setData('commerce_mercadopago_checkout', [
      'preference_id' => $request->query->get('preference_id'),
    ]);
    $order->set('payment_method', $payment_method);
    $order->save();

    $payment_storage = $this->entityTypeManager->getStorage('commerce_payment');
    $payment = $payment_storage->create([
      'state' => $request->query->get('status') == 'approved' ? 'authorization' : 'new',
      'amount' => $order->getBalance(),
      'payment_gateway' => $this->parentEntity->id(),
      'payment_method' => $payment_method->id(),
      'order_id' => $order->id(),
    ]);
    $payment->setRemoteId($request->query->get('payment_id'));
    $payment->save();
  }

}
