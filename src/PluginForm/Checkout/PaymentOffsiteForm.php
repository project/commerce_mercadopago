<?php

namespace Drupal\commerce_mercadopago\PluginForm\Checkout;

use Drupal\commerce_mercadopago\SmartPaymentButtonsBuilderInterface;
use Drupal\commerce_payment\PluginForm\PaymentOffsiteForm as BasePaymentOffsiteForm;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides the Off-site form for MercadoPago Checkout.
 *
 * This is provided as a fallback when no "review" step is present in Checkout.
 */
class PaymentOffsiteForm extends BasePaymentOffsiteForm implements ContainerInjectionInterface {

  /**
   * The Smart payment buttons builder.
   *
   * @var \Drupal\commerce_mercadopago\SmartPaymentButtonsBuilderInterface
   */
  protected $builder;

  /**
   * Constructs a new PaymentOffsiteForm object.
   *
   * @param \Drupal\commerce_mercadopago\SmartPaymentButtonsBuilderInterface $builder
   *   The Smart payment buttons builder.
   */
  public function __construct(SmartPaymentButtonsBuilderInterface $builder) {
    $this->builder = $builder;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('commerce_mercadopago.smart_payment_buttons_builder')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    /** @var \Drupal\commerce_payment\Entity\PaymentInterface $payment */
    $payment = $this->entity;
    $return_url = $form['#return_url'];
    $form['mercadopago_smart_payment_buttons'] = $this->builder->build($payment, $return_url, TRUE);
    return $form;
  }

}
